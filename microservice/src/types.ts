//code generate automatic not edit date: 2023-08-25T01:05:46.749Z

import { uri, uuid } from "onbbu";

//@onbbu-name: alfafores
export const name: string = "jtvmgxiqfpdttzb";

export interface ModelAttributes {
	id: number

	name: string
	precio: number
	variedad: string
	marca: uuid
	imagen: uri
	homepage: uri

	createdAt: string
	updatedAt: string
}

export interface Create {
	name: string
	precio: number
	variedad: string[]
	marca: uuid
	imagen: uri
	homepage: uri
}

export interface Update {
	where: {
		id: number
	}
	params: {
		name?: string
		precio?: number
		variedad?: string
		marca?: uuid
		imagen?: uri
		homepage?: uri
	}
}

export interface Destroy {
	where: {
		id: number[]
		marca: uuid[]
	}
}

export interface FindAndCount {
	where: {
		variedad: string
		marca: uuid
	}
	paginate: {
		limit?: number
		offset?: number
	}
}








export const endpoint = [
	'create',
	'update',
	'destroy',
	'findAndCount',
] as const;

export type Endpoint = typeof endpoint[number]