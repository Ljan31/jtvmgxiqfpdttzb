//code generate automatic not edit date: 2023-08-25T01:10:58.578Z
import Models, { DataTypes, S } from 'onbbu/models';
import * as T from '../types';

const model: Models<Partial<T.ModelAttributes>> = new Models<Partial<T.ModelAttributes>>(T.name);

model.define({

	attributes: {

		name: DataTypes.STRING,

		precio: DataTypes.BIGINT,

		variedad: DataTypes.STRING,

		marca: DataTypes.UUID,

		imagen: DataTypes.STRING,

		homepage: DataTypes.STRING,

	},
	options: { freezeTableName: true }
});

export const sync = (props: S.SyncOptions) => model.sync(props);

export default model;