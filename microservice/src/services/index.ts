//code generate automatic not edit date: 2023-08-25T01:15:44.118Z
import model from '../models';
import * as T from '../types';
import { NotFoundError, Paginate, Response } from 'onbbu';

export const create = async (params: T.Create): Response<Partial<T.ModelAttributes>[]> => {

	const instances: Partial<T.ModelAttributes>[] = params.variedad.map((variedad: string) => ({
		variedad, 
		name: params.name,
		precio: params.precio,
		marca: params.marca,
		imagen: params.imagen,
		homepage: params.homepage
	}))

	const data = await model.bulkCreate(instances)

	return { statusCode: "success", data: data };

};

export const update = async ({ where, params }: T.Update): Response<Partial<T.ModelAttributes>[]> => {

	const { affected, instances } = await model.update(params, { where })

	if (affected === 0) throw new NotFoundError("not found")

	return { statusCode: 'success', data: instances };
};

export const destroy = async ({ where }: T.Destroy): Response<Partial<T.ModelAttributes>[]> => {

	const { affected, instances } = await model.destroy({ where })

	if (affected === 0) throw new NotFoundError("not found")

	return { statusCode: "success", data: instances };
};

export const findAndCount = async ({ where, paginate }: T.FindAndCount): Response<Paginate<Partial<T.ModelAttributes>>> => {

	const { data, itemCount, pageCount } = await model.findAndCountAll({ where, ...paginate })

	if (data.length === 0) return { statusCode: "success", data: { data: [], itemCount: 0, pageCount: 0 } };

	return { statusCode: "success", data: { data, itemCount, pageCount } };
};