//code generate automatic not edit date: 2023-08-25T01:10:58.587Z
import Joi from 'onbbu/validate';
import * as T from '../types';

export const create = async (params: T.Create): Promise<T.Create> => {

	const schema = Joi.object({
		name: Joi.string().max(255).required(),
		precio: Joi.number().min(0).required(),
		variedad: Joi.string().max(255).required(),
		marca: Joi.string().uuid().required(),
		imagen: Joi.string().uri().required(),
		homepage: Joi.string().uri().required(),
	})

	await schema.validateAsync(params);

	return params;
};

export const update = async (params: T.Update): Promise<T.Update> => {

	const schema = Joi.object({
		where: Joi.object({
			id: Joi.number().min(0).required(),
		}).required(),
		parmas: Joi.object({
			name: Joi.string().max(255),
			precio: Joi.number().min(0),
			variedad: Joi.string().max(255),
			marca: Joi.string().uuid(),
			imagen: Joi.string().uri(),
			homepage: Joi.string().uri(),
		}).required(),
	})

	await schema.validateAsync(params);

	return params;
};

export const destroy = async (params: T.Destroy): Promise<T.Destroy> => {

	const schema = Joi.object({
		where: Joi.object({
			id: Joi.array().items(Joi.number().min(0).required().required()).min(1).required(),
			marca: Joi.array().items(Joi.string().uuid().required().required()).min(1).required(),
		}).required(),
	})

	await schema.validateAsync(params);

	return params;
};

export const findAndCount = async (params: T.FindAndCount): Promise<T.FindAndCount> => {

	const schema = Joi.object({
		where: Joi.object({
			variedad: Joi.string().max(255).required(),
			marca: Joi.string().uuid().required(),
		}).required(),
		paginate: Joi.object({
			limit: Joi.number().min(0),
			offset: Joi.number().min(0),
		}).required(),
	})

	await schema.validateAsync(params);

	return params;
};