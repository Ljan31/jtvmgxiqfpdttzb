//code generate automatic not edit date: 2023-08-25T01:10:58.595Z
import dotenv from "dotenv";
import Subscribe from "onbbu/subscribe";

import jtvmgxiqfpdttzb from "@api/jtvmgxiqfpdttzb";

dotenv.config();

const subscribe: Subscribe = new Subscribe();

/**Example:
subscribe.use("microservice::method", async (instance: TYPE.ModelAttributes) => {
	//Your function here
});
*/

const start = async (): Promise<void> => subscribe.run();

const stop = async (): Promise<void> => subscribe.stop();

export default { start, stop };